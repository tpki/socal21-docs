# README


This is the repository for an event calender of "Landesarbeitgemschaft Soziokultur in Schleswig-Holstein e.V.".

It currently contains of

* standardized API based upon [Schema.org](https://schema.org)
* taxonomies the project uses

If you have any further questions please contact admin@soziokultur-sh.de

The rest of the content currently is only avaliable in German [LIESMICH.md](./LIESMICH.md), sorry.
