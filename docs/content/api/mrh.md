---
title: "XML Veranstaltungs API"
date: 2021-12-16
lastmod: 2022-2-3
suchbereich: "performance"
categories: 
- Veranstaltungen
tags:
 - MRH
 - XML
draft: false
---

Die XML API für u.a. die Metropolregion Hamburg (MRH) wird von der Hamburg Tourismus GmbH  für die Veranstaltungsdatenbank (VADB) der Hansestadt und Metropolregion genutzt (https://www.mrh.events/) Diese Veranstaltungsdatenbank tauscht Veranstaltungstermine mit vielen weiteren Veranstaltungsdatenbank in Schleswig-Holstein und wird auch auf vielen Websites via Widgets ausgespielt.

Hier funktioniert der Austausch über XML. Weiterführende Details findet man auf der Seite [Technische Informationen](https://www.mrh.events/veranstaltungsausgabe/technische-informationen.html) (zB auch Handbücher unter "Downloads" auf der Seite)

Wir verwenden die Schnittstelle aufgrund ihrer Verbreitung und Akzeptanz auch zum Exportieren an weitere Veranstaltungsdatenbanken.

Termine in der VADB kann man hier suchen: https://www.veranstaltungen.hamburg/


## XML Schema (XSD)

Wir haben ein inoffizielles XML Schema erstellt, um unsere Exportdateien zu überprüfen. 

* Zum Download [mrh-lag-2022-1.xsd](https://socal21.gitlab.io/socal21-docs/mrh-lag-2022-1.xsd)


## Öffentliche API

Der öffentliche API-Key lautet: {{< param apikey >}}

* Open Data MRH API URL: [{{< param apiurl >}}api?apiKey={{< param apikey >}}&search={{< param suchbereich >}}&type=2328&style=xml]( {{< param apiurl >}}api?apiKey={{< param apikey >}}&search={{< param suchbereich >}}&type=2328&style=xml)

