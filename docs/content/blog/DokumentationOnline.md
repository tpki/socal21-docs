---
title: "Dokumentation Online"
date: 2021-11-02
categories: ["Public API"]
draft: false
---

Die existierenden Dokumenationen für die "Public API" wurden nun zum größten Teil zusammengetragen und hier als Website in neuer Form veröffentlicht. Diese werden ab sofort erweitert und bei Änderungen der Schnittstelle angepasst.

