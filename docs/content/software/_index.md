---
title: "Software"
date: 2021-11-29
lastmod: 2022-02-15
draft: false
---
Welche Softwarekomponenten kommen im SozioKulturKalender zum Einsatz?
Für die Nutzung der API sind auch andere Softwarekomponenten denkbar. Diese Softwareauswahl dokumentiert die konkrete Instanz für die LAG Soziokultur SH

Die Software zu dem Projekt wird ebenfalls auf Gitlab veröffentlicht: https://gitlab.com/socal21/socal21


## Softwarekomponenten

* Betriebssystem: Ubuntu Linux LTS (diverse Lizenzen)
* Content Management System: [TYPO3](/software/typo3) (Lizenz: GPL v2)
* Programmiersprache: PHP (siehe [Wikipedia](https://de.wikipedia.org/wiki/PHP)) (Lizenz: PHP-Lizenz)
* Datenbank: MySQL (Lizenz: GPL v2)
* Webserver: Apache (Lizenz: Apache Lizenz 2.0)
* [digiCULT.xTree](/software/xtree)
