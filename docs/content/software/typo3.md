---
title: "TYPO3"
date: 2021-11-30
lastmod: 2023-01-25
draft: false
keywords: ["TYPO3"]
---

## Beschreibung

**TYPO3 CMS** ist ein freies Content-Management-System für Websites, das seit Oktober 2012 offiziell unter dem Namen TYPO3 CMS angeboten wird. Ursprünglich wurde TYPO3 von Kasper Skårhøj entwickelt. Der Kern von TYPO3 ist in der Skriptsprache PHP geschrieben, die Ausgabe im Browser erfolgt mit HTML und JavaScript.

Die Installation und Konfiguration von TYPO3 wurde von der Firma JUSTORANGE vorgenommen.

Den Code für das Plugin findet man auf Gitlab: 
* https://gitlab.com/socal21/socal21/-/tree/main/Typo3/typo3conf/ext/theatercollection



### TypoScript

TypoScript ist eine Konfigurationssprache von Typo3 und erlaubt zB das flexible Gestalten von Layouts und Schnittstellen.

In unserem Fall wurde TypoScript zur Konfiguration der Eingabemaske verwendet und der [API](/api/)s

## Verwendung im Projekt

* TYPO3 stellt das Backend für verschiedene Bereiche der Lösung.
* Bereitstellung einer Eingabemaske für Mitglieder und Projekte der LAG Soziokultur SH
* Öffentliche API
* XML-Export an die **Metropolregion Hamburg** und als JSON an **Termine Regional**

## Über JUSTORANGE

JUSTORANGE ist eine Fullservice Werbeagentur aus Jena.

* Website: https://justorange.de/
