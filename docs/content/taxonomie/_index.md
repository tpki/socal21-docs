---
title: "Taxonomie"
date: 2021-11-02
draft: false
weight: 4
---

Die Taxonomien sind nicht Teil der API oder der Software, sondern projektbezogen. Daher bezieht sich  dieser Teil der Dokumentation explizit auf die Implementation der API mit Kategorien, Merkmalen, Suchkriterien, etc. auf die Soziokultur in Schleswig-Holstein. Vieles unserer Taxonomien haben wir von der Metropolregion Hamburg übernommen, die für unseren Start sehr hilfreich war. Teile der Texte und Definitionen durften wir auch übernehmen. Zum anderen haben wir uns aus verschiedenen anderen Projekten inspirieren lassen. Und wir haben unsere Taxonomie in Abstimmung mit digiCULT vorgenommen und in digiCULT.xTree angelegt, so dass diese auch als Grundlage für ähnliche Projekte dienen kann.

